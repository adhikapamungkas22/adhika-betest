import app from './app.js'

const PORT = process.env.PORT || 3001;
const startServer = async () => {
    try {
      app.listen(PORT, () => {
        console.log(`Server running on port ${PORT}`);
      });
    } catch (err) {
      console.error('Unable to connect to the database:', err);
    }
};
  
startServer();