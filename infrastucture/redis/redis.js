import { createClient } from 'redis';

// Redis client
const redisClient = createClient({
    host: 'localhost',
    port: 6379,
    legacyMode: true 
});

redisClient.on('error', (err) => {
  console.error('Redis client error:', err);
});


export default redisClient;