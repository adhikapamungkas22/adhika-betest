import mongoose from 'mongoose';
import dotenv from 'dotenv';

dotenv.config();

// set url mongoDB
const mongoConfig = process.env.DB_DRIVER + '://' + process.env.MONGODB_HOST + ":" 
+ process.env.MONGODB_PORT + "/" + process.env.MONGODB_DBNAME

// MongoDB connection
const mongooseClient = mongoose.connect(mongoConfig, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

export default mongooseClient;