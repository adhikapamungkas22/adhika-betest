# Use the official Node.js image from the Docker Hub
FROM node:18

# create dir
RUN mkdir -p /usr/app
# Set the working directory in the container
WORKDIR /usr/app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install the dependencies
RUN npm install

# Copy the rest of the application code
COPY . .

# Expose the application port
EXPOSE 3001

# Define environment variable
ENV NODE_ENV=development

# Start the application
CMD ["node", "index.js"]
