import Router from 'express';
import { login, createAccount, getAllAccount, getLastAccountLogin } from '../controllers/authContoller.js'
import loginValidator from '../validator/auth.js'
import { validateRequest } from '../middlewares/validateRequest.js';
import { protect } from '../middlewares/auth.js';
const router = Router();

// auth account
router.post('/v1/auth/login', loginValidator, validateRequest, login);
router.post('/v1/account/create', createAccount)
router.get('/v1/account', protect, getAllAccount)
router.get('/v1/account/get-last-login',protect, getLastAccountLogin)

export default router;