import Router from 'express';
import {
    createUser,
    getAllUsers,
    getUserById,
    updateUser,
    deleteUser,
    getUserByAccountNumber,
    getUserByRegistrationNumber
} from '../controllers/userController.js';
import { protect } from '../middlewares/auth.js';
// import cors from 'cors';
// import corsOptions from '../cors/cors.js'

const router = Router();
// set cors
// router.use(cors(corsOptions));

//users 
router.post('/v1/users/create', createUser)
router.get('/v1/users', protect, getAllUsers);
router.get('/v1/users/:id', getUserById);
router.put('/v1/users/:id/update', updateUser);
router.delete('/v1/users/:id', protect, deleteUser);
router.get('/v1/users/get-by-account-number/:accountNumber', getUserByAccountNumber)
router.get('/v1/users/get-by-registration-number/:registrationNumber', getUserByRegistrationNumber)

export default router;