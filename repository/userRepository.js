import User from '../models/user.js';
import utils from '../utils/utils.js';

class UserService {
  async createUser(data) {
    const user = new User({
      userId: data.userId,
      fullName : data.fullName,
      emailAddress: data.emailAddress,
      registrationNumber : data.registrationNumber,
      accountNumber: data.accountNumber,
      registrationNumber : utils.generateRegistrationNumber()
    });

    return await user.save();
  }

  async getAllUsers() {
    return await User.find();
  }

  async getUserById(id) {
    return await User.findById(id);
  }

  async updateUser(id, data) {
    await User.findByIdAndUpdate(id, data);
    return await this.getUserById(id);
  }

  async deleteUser(id) {
    await User.findByIdAndDelete(id);
  }

  async getUserByEmail(email) {
    return await User.findOne({ emailAddress : email });
  }

  async getUserByAccountNumber(accountNumber) {
    return await User.findOne({ accountNumber : accountNumber });
  }

  async getUserByRegistrationNumber(registrationNumber) {
    return await User.findOne({ registrationNumber : registrationNumber });
  }
}

export default new UserService();
