import Account from '../models/account.js';

class AccountService {
    async createAccount(data) {
        const account = new Account({
            userId: data.userId,
            userName : data.userName,
            password : data.password
        });

        return await account.save();
    }

    async getAllAccount() {
        return await Account.find();
    }

    async getAccountById(id) {
        return await Account.findById(id);
    }

    async updateAccount(id, data) {
        await Account.findByIdAndUpdate(id, data);
        return await this.getAccountById(id);
    }

    async deleteAccount(id) {
        await Account.findByIdAndDelete(id);
    }

    async getAccountByUserId(userId) {
        return await Account.findOne({userId : userId});
    }

    async getAccountByUsername(userName) {
        return await Account.findOne({userName : userName});
    }

    async getlastLoginDateTime() {
        return await Account.find(
            {
                lastLoginDateTime : 
                {
                    $gte: new Date((new Date().getTime() - (3 * 24 * 60 * 60 * 1000)))
                }
            }
        ).sort({ lastLoginDateTime: -1 })
    }
}

export default new AccountService();
