import { expect } from 'chai';
import sinon from 'sinon';
import userController from '../../controllers/userController';
import userService from '../../services/userService';

describe('userController', () => {
  afterEach(() => {
    sinon.restore();
  });

  it('should create an user', async () => {
    const user = { key: 'testKey', value: 'testValue' };
    const stub = sinon.stub(userRepository, 'create').resolves(user);

    const result = await userService.createUser(user);
    expect(result).to.equal(user);
    expect(stub.calledOnce).to.be.true;
  });

});
