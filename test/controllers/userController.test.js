import { chai } from 'chai';
import chaiHttp from 'chai-http';
import sinon from 'sinon';
import app from '../../app.js';
import userService from '../../controllers/userController.js';

chai.use(chaiHttp);
const { expect } = chai;

describe('UserController', () => {
  afterEach(() => {
    sinon.restore();
  });

  it('should create an user', (done) => {
    const user = { key: 'testKey', value: 'testValue' };
    const stub = sinon.stub(userService, 'createUser').resolves(user);

    chai.request(app)
      .post('/api/v1/users')
      .send(user)
      .end((err, res) => {
        expect(res).to.have.status(201);
        expect(res.body).to.deep.equal(user);
        expect(stub.calledOnce).to.be.true;
        done();
      });
  });
});
