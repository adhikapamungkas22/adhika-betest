 
 const generateRegistrationNumber = () => {
    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth();
    const dt = date.getDate();
    const seconds = date.getSeconds();
    const registrationNumber = year +""+ month +"" + dt + ""+ seconds

    return registrationNumber
}

export default  { generateRegistrationNumber }
