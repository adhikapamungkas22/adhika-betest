class ResponseHandler {
    success(res, data, message = 'Success') {
      res.status(200).json({
        success: true,
        message,
        data
      });
    }
  
    error(res, message) {
      res.status(500).json({
        success: false,
        message
      });
    }
  
    notFound(res, message = 'Resource not found') {
      this.error(res, message, 404);
    }

    badRequest(res, message) {
      if (message == "" ) {
        message = 'User not found'
      }
      this.error(res, message, 400);
    }
  }
  
export default new ResponseHandler();
  