import express    from 'express';
import bodyParser from 'body-parser';
import userRoutes from './routes/users.js';
import accountRoute from './routes/account.js'
import { errorHandler } from './middlewares/errorHandler.js';
import mongoose from './infrastucture/mongo/client.js';
import dotenv from 'dotenv';
import redisClient from './infrastucture/redis/redis.js'

dotenv.config();

const app = express();
// mongoDB init
await mongoose.connection;

// Redis init
await redisClient.connect();

// Middleware
app.use(bodyParser.json());
// Error Handling Middleware
app.use(errorHandler)

// Routes
app.use('/api', userRoutes);
app.use('/api', accountRoute)

export default app;