import { body } from 'express-validator'

const loginValidator = [
//   body('emailAddress', 'Invalid does not Empty').not().isEmpty() || body('userName', 'Invalid does not Empty').not().isEmpty(),
//   body('emailAddress', 'Invalid email').isEmail(),
  body('password', 'The minimum password length is 6 characters').isLength({min: 6}),
]
export default loginValidator