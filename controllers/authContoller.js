import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import responseHandler from '../utils/responseHandler.js';
import userRepository from '../repository/userRepository.js';
import accountRepository from '../repository/accountRepository.js';


export const login = async (req, res) => {
    try {
        const { emailAddress, userName,  password } = req.body;
        let passwordFromDB = ""
        let accountId = ""
       if (emailAddress != undefined) {
            // if login use email
            const user = await userRepository.getUserByEmail(emailAddress);
            if (!user) {
                return responseHandler.badRequest(res, 'User not found');
            }

            // find account by userId for get password
            const findAccount = await accountRepository.getAccountByUserId(user._id.toString());
            if (!findAccount) {
                return responseHandler.badRequest(res, 'User not found getAccountByUserId');
            }

            passwordFromDB = findAccount.password
            accountId = findAccount._id.toString();
       } else {
            const account = await accountRepository.getAccountByUsername(userName);
            if (!account) {
                return responseHandler.badRequest(res, 'Account not found getAccountByUsername');
            }
            // find userId for check already exist
            const findAccount = await userRepository.getUserById(account.userId);
            if (!findAccount) {
                return responseHandler.badRequest(res, 'User not found getUserById');
            }

            passwordFromDB = account.password
            accountId = account._id.toString();
       }

        const isMatch = await bcrypt.compare(password, passwordFromDB);
        if (!isMatch) {
            return responseHandler.badRequest(res, 'Invalid credentials');
        }

        const token = createToken(accountId);
        const data = { emailAddress, userName, token };
        return responseHandler.success(res, data, 'User login successfully');
    } catch (error) {
        return responseHandler.error(res, error.message);
    }
};

export const createAccount = async (req, res) => {
    try {
        const findByUsername = await accountRepository.getAccountByUsername(req.body.userName)
        if (findByUsername != null){
            return responseHandler.badRequest(res, 'Username already exist');
        } else {
            const user = await accountRepository.createAccount(req.body);
            return responseHandler.success(res, user, 'Account created successfully');
        }
    } catch (error) {
        return responseHandler.error(res, error.message);
    }
}

export const getAllAccount =  async (req, res) => {
    try {
      const accounts = await accountRepository.getAllAccount();
      const resp = mappingUser(accounts)
      return responseHandler.success(res, resp);
    } catch (error) {
      return responseHandler.error(res, error.message);
    }
}


export const getLastAccountLogin = async (req, res) => {
    try {
        const accounts = await accountRepository.getlastLoginDateTime();
        const resp = mappingUser(accounts)
        return responseHandler.success(res, resp)
    } catch (error) {
        return responseHandler.error(res, error.message);
    }
}


function createToken(accountId) {
    console.log("params create token", accountId)
    const token =  jwt.sign({ 
        accountId: accountId
        }, 
            process.env.JWT_SECRET, {
            expiresIn: '1h',
    });

    return token
}

function newMappingAccount(accounts){
    const arr = []
    const ns = new Object();
    accounts.map(function(account) { 
        ns.accountId = account._id
        ns.userId = account.userId
        ns.accountId = account.accountId
        ns.userName = account.userName
        ns.lastLoginDateTime = account.lastLoginDateTime

        arr.push(ns)
    });

    return arr
}