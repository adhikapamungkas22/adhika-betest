import userRepository from '../repository/userRepository.js';
import responseHandler from '../utils/responseHandler.js';
import redisClient from '../infrastucture/redis/redis.js';
import dotenv from 'dotenv';

dotenv.config();

export const createUser = async (req, res) => {
    try {
        // check user by email address 
        const email =  req.body.emailAddress
        const findUserByEmail = await userRepository.getUserByEmail(email)
        if (findUserByEmail != null) {
            return responseHandler.badRequest(res, 'User already exist');
        } 
      
        // check accountNumber
        const accountNumber = req.body.accountNumber
        const findAccountNumber = await userRepository.getUserByAccountNumber(accountNumber)
        if (findAccountNumber != null) {
            return responseHandler.badRequest(res, 'Account number already exist');
        } else {
            const user = await userRepository.createUser(req.body);
            await redisClient.del('allusers');
            return responseHandler.success(res, user, 'User created successfully');
        }
    } catch (error) {
        return responseHandler.error(res, error.message);
    }
}

export const getAllUsers =  async (req, res) => {
    try {
      const cachedUsers = await redisClient.get('allusers');
      if (cachedUsers) {
        return responseHandler.success(res, cachedUsers);
      }else{
        const users = await userRepository.getAllUsers();
        const newUserMapping = mappingUser(users)
        redisClient.setEx('allusers', 1800, newUserMapping);
        return responseHandler.success(res, newUserMapping)
      }
    } catch (error) {
      return responseHandler.error(res, error.message);
    }
}

export const getUserById = async (req, res) => {
  try {
    const cachedUser = await redisClient.get(req.param.id);
    if (cachedUser) {
      return responseHandler.success(res, cachedUser);
    }

    const user = await userRepository.getUserById(req.params.id);
    if (user) {
      redisClient.setEx(req.param.id, 1800, user);
      return responseHandler.success(res, user);
    } else {
      return responseHandler.notFound(res, 'User not found');
    }
  } catch (error) {
    responseHandler.error(res, error.message);
  }
}

export const updateUser = async (req, res) => {
  try {
    const user = await userRepository.updateUser(req.params.id, req.body);
    return responseHandler.success(res, user, 'User updated successfully');
  } catch (error) {
    return responseHandler.error(res, error.message);
  }
}

export const deleteUser = async (req, res) =>{
  try {
    await userRepository.deleteUser(req.params.id);
    return responseHandler.success(res, null, 'User deleted successfully');
  } catch (error) {
    return responseHandler.error(res, error.message);
  }
}


export const getUserByAccountNumber = async (req, res) => {
  try {
    console.log("account Number", req.params)
    const user = await userRepository.getUserByAccountNumber(req.params.accountNumber);
    if (user) {
      return responseHandler.success(res, user);
    } else {
      return responseHandler.notFound(res, 'User not found');
    }
  } catch (error) {
    responseHandler.error(res, error.message);
  }
}

export const getUserByRegistrationNumber = async (req, res) => {
  try {
    const user = await userRepository.getUserByRegistrationNumber(req.params.registrationNumber);
    if (user) {
      return responseHandler.success(res, user);
    } else {
      return responseHandler.notFound(res, 'User not found');
    }
  } catch (error) {
    return responseHandler.error(res, error.message);
  }
}

function mappingUser(users){
  const arr = []
  const ns = new Object();
  users.map(function(user) { 
      ns.userId = user._id
      ns.fullName = user.fullName
      ns.emailAddress = user.emailAddress
      ns.registrationNumber = user.registrationNumber
      ns.accountNumber = user.accountNumber

      arr.push(ns)
  });

  return arr
}