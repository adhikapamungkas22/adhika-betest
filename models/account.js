import bcrypt from 'bcryptjs';
import mongoose from 'mongoose';

const accountSchema = new mongoose.Schema({
    userName:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    lastLoginDateTime:{
        type:Date,
        default:Date.now
    },
    userId : {
        type:String,
        required:true
    },
});

accountSchema.pre('save', async function(next) {
    const account = this;
    const salt = await bcrypt.genSalt(10);
    account.password = await bcrypt.hash(account.password, salt);
    next();
});

const Account = mongoose.model('Accounts', accountSchema);
export default Account;
