import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
    fullName:{
        type:String,
        required:true
    },
    accountNumber:{ 
        type:String,
        required:true
    },
    emailAddress:{
        type:String,
        required:true
    },
    registrationNumber:{
        type: String,
        required:true
    },
    createdAt:{
        type:Date,
        default:Date.now
    },
    updatedAt : {
        type:Date,
        default:Date.now
    },
});


const User = mongoose.model('Users', userSchema);
export default User;
