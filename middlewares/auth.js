import jwt from 'jsonwebtoken';

export const protect = (req, res, next) => {
    let token = ""
    if (req.header('Authorization') == undefined) {
        return res.status(401).json({ error: 'No authorization, use key authorization in header' });
    }
    token = req.header('Authorization').replace('Bearer ', '');
    if (!token) {
        return res.status(401).json({ error: 'No token, authorization denied' });
    }

  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    req.user = decoded.id;
    next();
  } catch (err) {
    res.status(401).json({ error: 'Token is not valid' });
  }
};
